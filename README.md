# Icon generator

L'image s'enregistre bien en tant qu'image mais n'a pas l'extension. Sur MacOS, TextEdit l'ouvre bien en tant qu'image, ou bien, il suffit d'ajouter une extension à la main pour qu'elle soit reconnu en tant qu'image n'importe où.

## Exemples de deux images resdimensionnées avec un fond et les bords plus our moins arrondis
![first image](./images/download.png)
> Première image

![second image](./images/download-1.png)
> Seconde image

![troisième image](./images/download-ranger.png)
> Troisième image (utilisant la règle range)
>
![quatrième image](./images/download-transp.png)
> Quatrième image (utilisant la règle range et en laissant aucun fond)

