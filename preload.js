let imageBase64;
const htmlToImage = require('html-to-image');

const observe = () => {
  const imageLoader = document.getElementById('imageLoader');
  imageLoader.addEventListener('change', handleImage, false);
  const canvas = document.getElementById('imageCanvas');
  const ctx = canvas.getContext('2d');

  function handleImage(e){
    const reader = new FileReader();
    reader.onload = function(event){
      const img = new Image();
      img.onload = function(){
        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img,0,0);
      };
      img.src = event.target.result;
    };
    reader.readAsDataURL(e.target.files[0]);
  }

  const slider = document.getElementById("imageRange");
  const text_slider = document.getElementById("textImageRange");
  const inputH = document.getElementById('imageSizeH');
  const inputL = document.getElementById('imageSizeL');
  document.getElementById('valid-image-button').onclick = () => { downloadImage() };

  slider.oninput = function() {
    text_slider.innerHTML = this.value;
    resizeImageRange(this.value)
  };
  inputH.onchange = function() {
    resizeImage(inputH.value, inputL.value);
  };
  inputL.onchange = function() {
    resizeImage(inputH.value, inputL.value);
  };
};

const resizeImageRange = (size) => {
  const ranger = document.getElementById('imageRange');
  const width = document.getElementById('imageCanvas').width;
  const height = document.getElementById('imageCanvas').height;

  document.getElementById('imageCanvas').remove(); // Delete the old canvas
  const newCanvas = document.createElement('canvas'); // Create the new canvas
  newCanvas.id = 'imageCanvas'; // Set the same id as before...
  newCanvas.width = ranger.value; // ...With dimensions
  newCanvas.height = ranger.value / (width / height);
  document.getElementById('innerImage').appendChild(newCanvas); // Set the new canvas in the id

  const canvas = document.getElementById('imageCanvas'); // Prepare the canvas
  const context = canvas.getContext('2d');
  const image = new Image();

  image.onload = function() {
    context.drawImage(image, 0, 0, ranger.value, ranger.value / (width / height));
  };
  image.src = document.getElementById('blah').src; // Set the image source with an hidden image src (for base64)
  imageBase64 = document.getElementById('blah').src;
};

/**
 * Resize the image with the two inputs height and width
 * Every time the size is updated, recreates the canvas with the new size
 *
 * @param {number|string} h Height of image
 * @param {number|string} l Width of image
 */
const resizeImage = (h, l) => {
  document.getElementById('imageCanvas').remove(); // Delete the old canvas
  const newCanvas = document.createElement('canvas'); // Create the new canvas
  newCanvas.id = 'imageCanvas'; // Set the same id as before...
  newCanvas.width = l; // ...With dimensions
  newCanvas.height = h;
  document.getElementById('innerImage').appendChild(newCanvas); // Set the new canvas in the id

  const canvas = document.getElementById('imageCanvas'); // Prepare the canvas
  const context = canvas.getContext('2d');
  const image = new Image();

  image.onload = function() {
    context.drawImage(image, 0, 0, l, h);
  };
  image.src = document.getElementById('blah').src; // Set the image source with an hidden image src (for base64)
  imageBase64 = document.getElementById('blah').src;
};

const downloadImage = () => {
  const node = document.getElementById('imageCanvas');

  htmlToImage.toPng(node)
    .then(function (imageBase64) {
      const img = new Image();
      const bTemp = imageBase64;
      img.src = imageBase64;
      window.open(bTemp.replace(/^data:image\/[^;]+/, 'data:application/octet-stream'));
    })
    .catch(function (error) {
      console.error('oops, something went wrong!', error);
    });
};

window.addEventListener('DOMContentLoaded', observe);
