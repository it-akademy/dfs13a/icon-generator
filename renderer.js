document.getElementById('colorPicker').onchange = () => { changeBackgroundColor() };

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imageLoader").change(function() {
  readURL(this);
});

function changeBackgroundColor() {
  document.getElementById('imageCanvas').style.backgroundColor = document.getElementById('colorPicker').value;
}

function modifyRadius(radiusType) {
  if (radiusType === 'd') {
    document.getElementById('imageCanvas').style.borderRadius = '0';
  } else if (radiusType === 'a') {
    document.getElementById('imageCanvas').style.borderRadius = '30px';
  } else if (radiusType === 'r') {
    document.getElementById('imageCanvas').style.borderRadius = '50%';
  }
}
